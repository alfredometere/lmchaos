% Channel Capacity of a Logistics Map 
%       Classic chaos example. 
%       x(n+1) = r*x(n)*(1-x(n)) as r increases to 4.
%
% 2018-09-11
% Author: Alfredo Metere
% e-mail: alfredometere2@gmail.com
%
% Copyright 2018 Alfredo Metere
% Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
% The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
% USE WISELY

clear
close all
format long
format compact
N = 1; % r steps from initial to final.
a = 3.8001; % initial r
b = 3.8001; % final r. For values above 4, it starts diverging: why?!
rs = linspace(a,b,N); % Generated r values array

x0_start = 0.;
x0_end = 1-1.e-8;
Nx0 =1000;
xs = linspace(x0_start, x0_end, Nx0);
t = 100; % number of time steps
CC = zeros(t, length(rs)); % Channel Capacity
LE = zeros(t, length(rs)); % Lyapunov Exponent


% Loop through r
for Dr = 1:length(rs)
    r=rs(Dr);      % Assign current r value
    for Dx_0 = 1:length(xs) 
        x =zeros(t,1); % System 1
        xx=zeros(t,1); % System 2
        x(1)  = xs(Dx_0);
        %x(1)  = 0.1;    % initial state system 1. Value between [0,1]
        xx(1) = x(1)+1.e-8; % initial state system 2. Value between [0,1]

        for Dt = 2:t,  % Simulate time evolution of the system
%             x(Dt)  = r *  x(Dt-1) * (1 -  x(Dt-1)); % New state. System 1
%             xx(Dt) = r * xx(Dt-1) * (1 - xx(Dt-1)); % New state. System 2
            
            x(Dt)  = r *  x(Dt-1) * (1 -  x(Dt-1)); % New state. System 1
            xx(Dt) = r * xx(Dt-1) * (1 - xx(Dt-1)); % New state. System 2

        % Channel Capacity at instant Dt
            %CC(Dt,Dr) = log2(xx(Dt)/xx(Dt-1))*Dt/t;
            CC(Dt,Dx_0) = log2(1+abs(xx(Dt))/abs(xx(Dt-1)))*Dt/t;
            Dx0 = abs(x(Dt-1)-xx(Dt-1));
            Dxt = abs(x(Dt)-xx(Dt));
        % Lyapunov Exponent at instant Dt\
            %LE(Dt,Dr) = log2(Dxt/Dx0)*Dt/t;        
            LE(Dt,Dx_0) = log2(1+Dxt/Dx0)*Dt/t;       
        end
        myX{Dx_0} = x;
        myX1{Dx_0} = xx;
    end
end

figure('Name',' Phase Space Trajectory','NumberTitle','on');clf
plot(xx)
xlabel('Time (recursions)'),ylabel("System's State")
grid on

figure('Name','Separation Distance','NumberTitle','on')
semilogy(abs(xx-x))
grid on
xlabel('Time (recursions)'),ylabel('Separation distance')
% Plot the data
figure('Name','Channel Capacity','NumberTitle','on')
tt = [1:t];
ot = ones(t,1);
% This plots Channel Capacity
h =semilogy(tt,ot,tt,CC(:,1),'-');

hold
set(h,'markersize',15)
set(h,'linewidth',1)
% for i = 2:length(rs)
% h=plot(CC(:,i));
% set(h,'linewidth',1)
% end
for i = 2:length(xs)
    
h=semilogy(CC(:,i));

set(h,'linewidth',1)
end
xlabel('Time (recursions)'),ylabel('Channel Capacity (bits/recursion)');
% for i = 1:length(rs)
%     legendCell{i} = num2str(rs(i),'CC: r=%g');
% end
for i = 1:length(xs)
    legendCell{i} = num2str(xs(i),'CC: x(0)=%g');
end
% hold
grid on
figure('Name','Lyapunov Exponent','NumberTitle','on');
tt = [1:t];
ot = ones(t,1);
g = semilogy(tt,ot,tt,LE(:,1),'-');
hold
set(g,'markersize',15)
set(g,'linewidth',1)
% for i = 2:length(rs)
% g=plot(LE(:,i));
% set(g,'linewidth',1)
% end
for i = 2:length(xs)
g=semilogy(LE(:,i));
set(g,'linewidth',1)
end
xlabel('Time (recursions)'),ylabel('Lyapunov Exponent (bits/recursion)');
% for i = 1:length(rs)
%     legendCell1{length(rs)} = num2str(rs(i),'LE: r=%g');
% end
for i = 1:length(xs)
    legendCell1{length(xs)} = num2str(xs(i),'LE: x(0)=%g');
end
grid on
figure('Name','|CC - LE|','NumberTitle','on')
plot(CC-LE)
grid on
legendCell2{1} = num2str(rs(1),'|CC-LE|: r=%g');
legend('show');
legend(legendCell2);

MFE = zeros(Nx0,1);
KSE = zeros(Nx0,1);

for j = 1:Nx0
    for i = 1:length(CC(:,1))
        if (CC(i,j) > 1)
            MFE(j) = MFE(j) + CC(i,j);
        end
        if (LE(i,j) > 1)
            KSE(j) = KSE(j) + LE(i,j);
        end
    end
end

figure('Name','MF vs. KS Entropies','NumberTitle','on')
plot(MFE./t,KSE./t,'.')
axis square
grid on
xlabel('Metere-Friedland Entropy'),ylabel('Kolmogorov-Sinai Entropy')

