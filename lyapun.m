% lyap=zeros(1,1000);
% j=0;
% for(r=0:0.0001:4)
%     xn1=rand(1);
%     lyp=0;
%     j=j+1;
%     for(i=1:10000)
%         xn=xn1;
%         %logistic map
%         xn1=r*xn*(1-xn);
%         dxdt = r-2*r*xn1;
%        %wait for transient
% %        if(i>1)
%            % calculate teh sum of logaritm
%            lyp=lyp+log(abs(r-2*r*xn1));
% %        end
%     end
%     %calculate lyapun
%     lyp=lyp/10000;
%     lyap(j)=lyp;
% end
% r=3:0.001:4;
% plot(r,lyap);

close all
clear

Fx = zeros(10,3);
y = zeros(10,2);
rmin = 1;
rmax = 4;
Nr = 5;
rs = linspace(rmin,rmax,Nr);
xf = zeros(Nr,2);
for r = 1:length(rs)
    dr = rs(r)
    xf(r,1) = 1-1/dr;
    xf(r,2) = 1-1/dr;
    for x = 1:101
        dx = (x-1)/100;
        Fx(x,r,1) = dx;
        Fx(x,r,2) = dr*dx*(1-dx);
        y(x,1) = dx;
        y(x,2) = dx;
    end
end

figure('Name','Logistic Map. x vs. F(x)','NumberTitle','off')
grid on
axis square
xlabel('x'),ylabel('F(x) = rx(1-x)')
title('F(x) = rx(1-x)')
hold
plot(y(:,1),y(:,2),'-')
legendCell{1} = 'y=x';
for i=1:length(rs)
    legendCell{i+1} = num2str(rs(i),'F(x): r=%g');
    plot(Fx(:,i,1),Fx(:,i,2),'-');
end
for i=1:length(rs)
    j = i + length(rs)
    legendCell{j} = num2str(rs(i),'xf: r=%g');
    plot(xf(i,1),xf(i,2),'ko')
end

legend('show');
legend(legendCell);

    
